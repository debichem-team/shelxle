shelxle (1.0.1552-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/control (Build-Depends): Raise dh-compat to v13.
    (Standards-Version): Bump to 4.6.2.
  * d/copyright: Update copyright. Add missing information.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 07 Aug 2023 23:31:49 +0200

shelxle (1.0.1472-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/patches/disable_update_search.patch: Refresh.

 -- Michael Banck <mbanck@debian.org>  Sun, 15 Jan 2023 14:34:36 +0100

shelxle (1.0.1346-2) unstable; urgency=medium

  [ Pino Toscano ]
  * Remove the unused XPM icon; no more needed since the removal of the Debian
    menu in 1.0.748-1.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 27 Nov 2022 17:24:14 +1100

shelxle (1.0.1346-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 01 Dec 2021 00:13:36 +0100

shelxle (1.0.1320-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 20 Sep 2021 17:51:21 +0200

shelxle (1.0.1318-1) unstable; urgency=medium

  * New upstream release.
  * d/control (Standards-Version): Bump to 4.6.0.
  * d/copyright (Copyright): Update.
  * d/shelxle.1: Update manual page.
  * d/rules: Enable full hardening.
  * d/watch: Fix older-debian-watch-file-standard and raise to version 4.
  * d/patches/disable_update_search.patch: Refresh.
  * d/tests/control: Disable and fix backgrounded-test-command.
  * d/upstream/metadata (Homepage): Fix upstream-metadata-field-unknown.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 14 Sep 2021 09:11:26 +0200

shelxle (1.0.1184-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 18 Nov 2020 20:43:44 +0100

shelxle (1.0.1179-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 16 Nov 2020 02:54:48 +0100

shelxle (1.0.1175-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 02 Nov 2020 22:09:16 +0100

shelxle (1.0.1172-1) unstable; urgency=medium

  * New upstream release.
  * d/patches/disable_update_search.patch: Fix patch-not-forwarded-upstream.
  * d/source/lintian-overrides: Initially add file. Override
    debian-watch-does-not-check-gpg-signature.
  * d/upstream/metadata: Add Bug-Submit field.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 14 Oct 2020 00:59:07 +0200

shelxle (1.0.1168-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 10 Oct 2020 16:48:05 +0200

shelxle (1.0.1161-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 05 Oct 2020 01:43:12 +0200

shelxle (1.0.1101-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 04 May 2020 15:04:32 +0200

shelxle (1.0.1083-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 10 Apr 2020 07:08:59 +0200

shelxle (1.0.1070-1) unstable; urgency=medium

  * New upstream release.
  * d/copyright: Update copyright and fix short license name.
  * d/tests/control: Add and run basic test.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 07 Mar 2020 21:26:04 +0100

shelxle (1.0.1062-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 10 Feb 2020 00:09:14 +0100

shelxle (1.0.1050-1) unstable; urgency=medium

  * New upstream release.
  * d/compat: Remove obsolete file.
  * d/control: Add Rules-Requires-Root: no.
    (Build-Depends): Fix uses-debhelper-compat-file and use debhelper-compat.
    (Standards-Version): Bump to 4.5.0.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 03 Feb 2020 00:37:31 +0100

shelxle (1.0.1034-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 14 Jan 2020 11:27:23 +0100

shelxle (1.0.1030-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 03 Jan 2020 23:06:13 +0100

shelxle (1.0.1027-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 22 Dec 2019 20:29:28 +0100

shelxle (1.0.1022-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 12 Dec 2019 22:17:04 +0100

shelxle (1.0.1018-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 05 Dec 2019 20:51:31 +0100

shelxle (1.0.1014-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 27 Nov 2019 22:37:57 +0100

shelxle (1.0.1008-1) unstable; urgency=medium

  * New upstream release.
  * d/control (Standards-Version): Bump to 4.4.1.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 10 Nov 2019 21:15:43 +0100

shelxle (1.0.985-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 15 Aug 2019 23:09:32 +0200

shelxle (1.0.981-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 05 Aug 2019 23:12:57 +0200

shelxle (1.0.973-1) unstable; urgency=medium

  * New upstream release.
  * d/compat: Raised to level 12.
  * d/control (Build-Depends): Bumped debhelper version.
    (Standards-Version): Bumped to 4.4.0.
    (Description): Use HTTPs uri.
  * d/copyright: Ditto. Updated.
  * d/shelxle.1: Ditto.
  * d/upstream/metadata: Ditto.
  * d/watch: Ditto.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 19 Jul 2019 16:10:12 +0200

shelxle (1.0.952-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 15 Feb 2019 16:27:08 +0100

shelxle (1.0.949-3) unstable; urgency=medium

  * d/control (Build-Depends): Revert the last change.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 14 Feb 2019 02:22:30 +0100

shelxle (1.0.949-2) unstable; urgency=medium

  * d/control (Build-Depends): Replaced virtual libqt5opengl5-desktop-dev
    package by libqt5opengl5-dev.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 13 Feb 2019 15:57:21 +0100

shelxle (1.0.949-1) unstable; urgency=medium

  * New upstream release.
  * d/control (Standards-Version): Bumped to 4.3.0.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 11 Feb 2019 16:26:45 +0100

shelxle (1.0.912-1) unstable; urgency=medium

  * New upstream release.
    - Fix FTBFS due to round macro (closes: #896451).
  * debian/control (Standards-Version): Bumped to 4.1.4.
  * debian/rules: Remove --parallel switch from dh call (default).
  * debian/patches/disable_update_search.patch: Updated to really disable
    version checking.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 30 Apr 2018 18:36:14 +0200

shelxle (1.0.888-1) unstable; urgency=medium

  * New upstream release.
  * debian/control (Build-Depends): Use libqt5opengl5-desktop-dev instead of
    libqt5opengl5-dev (closes: #887458). Removed qt5-default to fix
    build-depends-on-metapackage.
  * debian/rules: Set QT_SELECT.
  * debian/patches/qt5.patch: Patch removed (merged into upstream).
  * debian/patches/series: Adjusted.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 17 Jan 2018 17:11:02 +0100

shelxle (1.0.886-2) unstable; urgency=medium

  * debian/control (Homepage): Updated old URL (closes: #866203).
    (Build-Depends): Change to QT5 build dependencies (closes: #875189).
  * debian/upstream/metadata: Updated some more old URLs.
  * debian/patches/qt5.patch: Added.
    - Fix a compiler error with QT5.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 16 Jan 2018 11:33:33 +0100

shelxle (1.0.886-1) unstable; urgency=medium

  * New upstream release.
  * debian/compat: Raised to level 11.
  * debian/control (Build-Depends): Raised debhelper version.
    (Standards-Version): Bumped to 4.1.3.
    (Vcs-Browser): Use secure URI.
  * debian/copyright: Minor updates (closes: #866203).
  * debian/patches/fix_typos.patch: Applied upstream and removed.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 14 Jan 2018 22:18:51 +0100

shelxle (1.0.816-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 22 Nov 2016 21:47:58 +0100

shelxle (1.0.813-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 24 Oct 2016 22:58:39 +0200

shelxle (1.0.807-1) unstable; urgency=medium

  * New upstream release.
    - Fixes FTBFS with GCC 6 (closes: #811906).
  * debian/control (Standards-Version): Bumped to 3.9.8.
  * debian/copyright: Updated.
  * debian/patches/fix_typos.patch: Added.
    - Fixes some typos discovered by lintian.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 02 Oct 2016 00:09:30 +0200

shelxle (1.0.748-1) unstable; urgency=medium

  * New upstream release.
  * debian/shelxle.menu: Dropped to fix command-in-menu-file-and-desktop-file.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 17 Nov 2015 20:15:31 +0100

shelxle (1.0.740-1) unstable; urgency=medium

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 29 Aug 2015 10:20:56 +0200

shelxle (1.0.720-2) unstable; urgency=medium

  * debian/control (Build-Depends): Removed libqtwebkit-dev, which is no
    longer needed after offline help has been removed (closes: #784526).

 -- Daniel Leidert <dleidert@debian.org>  Sun, 17 May 2015 08:21:02 +0200

shelxle (1.0.720-1) unstable; urgency=medium

  * New upstream release.
    - Offline help has been removed.
  * debian/control (Standards-Version): Bumped to 3.9.6.
  * debian/copyright: Updated.
  * debian/shelxle.doc-base, debian/shelxle.links: Removed. Offline help
    files have been removed by upstream.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 10 May 2015 12:07:43 +0200

shelxle (1.0.677-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream: Moved to debian/upstream/metadata.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 21 Sep 2014 19:55:30 +0200

shelxle (1.0.661-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Major update.
  * debian/control (Build-Depends): Removed fftw3 - upstream uses kissfft.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 18 Feb 2014 22:18:21 +0100

shelxle (1.0.657-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 21 Dec 2013 22:40:06 +0100

shelxle (1.0.655-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 15 Dec 2013 20:36:59 +0100

shelxle (1.0.652-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.5.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 13 Dec 2013 23:47:18 +0100

shelxle (1.0.649-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 20 Oct 2013 21:39:24 +0200

shelxle (1.0.647-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 14 Aug 2013 21:17:26 +0200

shelxle (1.0.645-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 28 Jul 2013 11:01:02 +0200

shelxle (1.0.634-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 20 Jun 2013 21:37:56 +0200

shelxle (1.0.632-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 16 Jun 2013 13:44:30 +0200

shelxle (1.0.629-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Vcs-Browser, Vcs-Svn): Fixed vcs-field-not-canonical.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 01 Jun 2013 14:52:09 +0200

shelxle (1.0.626-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 23 May 2013 23:46:07 +0200

shelxle (1.0.623-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.4.

 -- Daniel Leidert <dleidert@debian.org>  Mon, 06 May 2013 21:29:24 +0200

shelxle (1.0.564-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Removed DM-Upload-Allowed.
  * debian/shelxle.doc-base: Added to register shipped documentation.
  * debian/shelxle.links: Added to link shipped documentation.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 29 Jun 2012 20:25:29 +0200

shelxle (1.0.562-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 24 Jun 2012 18:29:37 +0200

shelxle (1.0.561-1) unstable; urgency=low

  * New upstream release.
  * debian/compat: Bump to 9 to enable hardening wrappers.
  * debian/control (Build-Depends): Bump dh version.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Fri, 22 Jun 2012 20:13:10 +0200

shelxle (1.0.557-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Minor update.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 28 May 2012 12:34:29 +0200

shelxle (1.0.554-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Fixed syntax error.
  * debian/upstream: More data added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 26 May 2012 13:42:19 +0200

shelxle (1.0.551-1) unstable; urgency=low

  * New upstream release.

  [ Andreas Tille ]
  * debian/upstream:
     - Use more BibTeX friendly syntax for authors
     - Differentiate between URL and eprint

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 15 May 2012 22:28:54 +0200

shelxle (1.0.548-1) unstable; urgency=low

  * New upstream release.
  * debian/upstream: New file with bibliographic information.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 03 Apr 2012 01:03:16 +0200

shelxle (1.0.544-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.3.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 20 Mar 2012 23:15:24 +0100

shelxle (1.0.540-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 20 Feb 2012 20:33:10 +0100

shelxle (1.0.537-1) unstable; urgency=low

  * New upstream release.
  * debian/shelxle.1: Update.
  * debian/patches/disable_update_search.patch: Added.
    - Disable the automatic update search.
  * debian/patches/series: Added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Wed, 15 Feb 2012 19:46:06 +0100

shelxle (1.0.513-1) unstable; urgency=low

  * Initial packaging (closes: #654964).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 07 Jan 2012 21:51:22 +0100
